import { Injectable, ɵConsole } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class IndexeddbService {
  constructor() {
    let db;
    const dbReq = indexedDB.open('myDatabase', 1);
    dbReq.onupgradeneeded = (event: any) => {
      db = event.target.result;
      const users = db.createObjectStore('users', { autoIncrement: true });
    };
    dbReq.onsuccess = (event: any) => {
      db = event.target.result;
    };
    dbReq.onerror = (event: any) => {
      alert('error opening database ' + event.target.errorCode);
    };
    console.log('IndexedDB service injected!');
  }

  addUser(form) {
    let db;
    const dbReq = indexedDB.open('myDatabase', 1);
    dbReq.onsuccess = (event: any) => {
      db = event.target.result;
      this.addSubFunction(db, form);
    };
  }

  addSubFunction(db, form) {
    const tx = db.transaction(['users'], 'readwrite');
    const store = tx.objectStore('users');

    const user = { ...form, timestamp: Date.now() };
    store.add(user);

    tx.oncomplete = () => {
      console.log('stored user!');
    };
    tx.onerror = event => {
      alert('error storing user' + event.target.errorCode);
    };
  }

  verifyUser(form): Observable<any> {
    return new Observable(res => {
      let db;
      const dbReq = indexedDB.open('myDatabase', 1);
      dbReq.onsuccess = (event: any) => {
        db = event.target.result;
        this.verifySubFunction(db, form).subscribe(e => res.next(e));
      };
    });
  }

  verifySubFunction(db, form): Observable<any> {
    return new Observable(subscriber => {
      let match = false;
      const tx = db.transaction(['users'], 'readonly');
      const store = tx.objectStore('users');
      const req = store.getAll();
      req.onsuccess = event => {
        const user = event.target.result;
        if (user) {
          // tslint:disable-next-line: prefer-for-of
          for (let i = 0; i < user.length; i++) {
            if (
              form.email === user[i].email &&
              form.password === user[i].password
            ) {
              match = true;
              const usuario = user[i];
              subscriber.next(usuario);
              return;
            }
          }
        } else {
          return false;
        }
      };
      req.onerror = event => {
        alert('error getting user 1 ' + event.target.errorCode);
      };
      subscriber.next(false);
    });
  }

  editUser(form): Observable<any> {
    return new Observable(res => {
      let db;
      const dbReq = indexedDB.open('myDatabase', 1);
      dbReq.onsuccess = (event: any) => {
        db = event.target.result;
        this.editSubFunction(db, form).subscribe(e => res.next(e));
      };
    });
  }

  editSubFunction(db, form): Observable<any> {
    return new Observable(subscriber => {
      const tx = db.transaction(['users'], 'readwrite');
      const store = tx.objectStore('users');
      let userKey;
      const user = JSON.parse(localStorage.user);

      console.log('edit');
      const getUsers = store.getAll();
      getUsers.onsuccess = e => {
        const users = e.target.result;
        console.log(users);
        if (users.length) {
          for (let i = 0; i < users.length; i++) {
            if (users[i].email === user.email) {
              users[i].email = form.email;
              users[i].phone = form.phone;
              users[i].name = form.name;
              users[i].lastname = form.lastname;
              users[i].id = form.id;
              userKey = i + 1;
              console.log('editing user');
            }
          }
          const updateUserRequest = store.put(users[userKey - 1], userKey);

          console.log(
            'The transaction that originated this request is ' +
              updateUserRequest.transaction
          );

          updateUserRequest.onsuccess = e => {
            localStorage.removeItem('user');
            localStorage.setItem('user', JSON.stringify(users[userKey - 1 ]));
          };
          const usuario = users;
          subscriber.next(usuario);
          return;
        } else {
          return false;
        }
      };
      getUsers.onerror = event => {
        alert('error getting user 1 ' + event.target.errorCode);
      };
      subscriber.next(false);
    });
  }
}

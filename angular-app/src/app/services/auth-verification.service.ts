import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthVerificationService {
  constructor() {}

  checkLogged() {
    return localStorage.user ? true : false;
  }
}

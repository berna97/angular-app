import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PostsService {

  posts: any;
  postsURL = 'https://jsonplaceholder.typicode.com/posts';


  constructor(private httpClient: HttpClient) { }

  public postsTitleCapitalize(posts: any) {
    return posts.map(post => {
      return {
        title: `${post.title.split(' ').map(el => `${el[0].toUpperCase()}${el.substring(1,el.length)}`).join(' ')}`,
        id: post.id
      };
    });
  }

  public getPosts() {
    return this.httpClient.get(this.postsURL)
              .pipe(map(posts => this.postsTitleCapitalize(posts)));
  }

  public getPostDetails(id: string) {
    return this.httpClient.get(`https://jsonplaceholder.typicode.com/posts/${id}`);
  }
}

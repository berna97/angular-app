import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup} from '@angular/forms';
import {Router} from '@angular/router';

import { regexValidator, emailRegex, phoneRegex, allLettersRegex, allNumbersRegex } from '../../shared/regex-validator.directive';
import { IndexeddbService } from '../../services/indexed-db.service';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss']
})
export class ViewComponent implements OnInit {
  signupForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private db: IndexeddbService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.signupForm = this.fb.group({
      email: [null, [regexValidator(emailRegex)]],
      password: [null, [Validators.required]],
      phone: [null, [regexValidator(phoneRegex)]],
      name: [null, [regexValidator(allLettersRegex)]],
      lastname: [null, [regexValidator(allLettersRegex)]],
      id: [null, [regexValidator(allNumbersRegex)]]
    });

    console.log(this.signupForm.get('email').valid);
  }

  signupUser() {
    console.log(this.signupForm.value);
    const registerUser = this.db.addUser(this.signupForm.value);
    this.router.navigate(['/login']);
  }
}

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import {PostsService} from '../../services/posts.service';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss']
})
export class ViewComponent implements OnInit {

  post;
  id;

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    // this.route.params.subscribe(paramsId => {
    //   this.id = paramsId.id;
    // });
    // this.postService.getPostDetails(this.id).subscribe((data) => {
    //   console.log(data);
    //   this.post = data;
    // });
    this.post = this.route.snapshot.data['postData'];
    console.log(this.post);
  }

}

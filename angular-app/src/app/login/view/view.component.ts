import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import {Router} from '@angular/router';

import {
  regexValidator,
  emailRegex
} from '../../shared/regex-validator.directive';
import { IndexeddbService } from '../../services/indexed-db.service';
@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss']
})
export class ViewComponent implements OnInit {
  noUser = false;
  loginForm: FormGroup;

  constructor(private fb: FormBuilder, private db: IndexeddbService, private router: Router) {}

  ngOnInit(): void {
    this.loginForm = this.fb.group({
      email: [null, [regexValidator(emailRegex)]],
      password: [null, [Validators.required]]
    });
  }

  loginUser() {
    const user = this.db.verifyUser(this.loginForm.value).subscribe(res => {
      res ? localStorage.setItem('user', JSON.stringify(res)) : this.noUser = true;
      res ? this.router.navigate(['/post']) : null;
    });
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PostRoutingModule } from './post-routing.module';
import { ViewComponent } from './view/view.component';


@NgModule({
  declarations: [ViewComponent],
  imports: [
    CommonModule,
    PostRoutingModule
  ]
})
export class PostModule { 
  constructor() {
    console.log('Post module loaded');
  }
}

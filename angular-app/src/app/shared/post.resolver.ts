import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';

import {PostsService} from '../services/posts.service';

@Injectable()
export class PostResolver implements Resolve<any>  {
  posts: any;

  constructor(private postsService: PostsService) {}

  resolve() {
    return this.postsService.getPosts();
  }
}

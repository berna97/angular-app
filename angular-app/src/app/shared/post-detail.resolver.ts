import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';

import { ActivatedRouteSnapshot } from '@angular/router';
import {PostsService} from '../services/posts.service';

@Injectable()
export class PostDetailResolver implements Resolve<any>  {
  constructor(private postsService: PostsService) {}

  resolve(route: ActivatedRouteSnapshot) {
    // console.log(route.paramMap.get('id'));
    // console.log(this.postsService.getPostDetails(route.paramMap.get('id')));
    return this.postsService.getPostDetails(route.paramMap.get('id'));
  }
}